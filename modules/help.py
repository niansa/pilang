"""
This file is part of pilang.

pilang is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pilang is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pilang.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys
import rtypes

licensenote = """pilang - a litttle, fun, weird programming language!
Copyright (C) 2020  niansa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>."""


class help:
    def __init__(self, environ):
        self.commands = {
            "help": self.cmd_help,
            "license": self.cmd_license,
            "copyright": self.cmd_license
            }
        self.environ = environ
    
    
    def processor(self, command, args):
        try:
            return self.commands[command](args)
        except KeyError:
            return errors.nochsuchcmd
    
    def cmd_help(self, args):
        return "No documentation yet.."
    
    def cmd_license(self, args):
        return licensenote


def adder(registerer):
    registerer(help)

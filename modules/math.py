"""
This file is part of pilang.

pilang is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pilang is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pilang.  If not, see <https://www.gnu.org/licenses/>.
"""
import errors
import rtypes


class math:
    def __init__(self, environ):
        self.commands = {
            "add": self.cmd_add,
            "sub": self.cmd_sub,
            "mul": self.cmd_mul,
            "div": self.cmd_div
            }

    def processor(self, command, args):
        try:
            return self.commands[command](args)
        except KeyError:
            return errors.nochsuchcmd

    def cmd_add(self, args):
        try:
            if args[0] == "asint":
                asint = True
                args = args[1:]
            else:
                asint = False
            result = 0
            for arg in args:
                result += float(arg)
            if asint: result = int(result)
            return result
        except ValueError:
            return errors.badarguments
    
    def cmd_sub(self, args):
        try:
            if args[0] == "asint":
                asint = True
                args = args[1:]
            else:
                asint = False
            result = float(args[0])
            for arg in args[1:]:
                result -= float(arg)
            if asint: result = int(result)
            return result
        except ValueError:
            return errors.badarguments
    
    def cmd_mul(self, args):
        try:
            if args[0] == "asint":
                asint = True
                args = args[1:]
            else:
                asint = False
            result = 1
            for arg in args:
                result *= float(arg)
            if asint: result = int(result)
            return result
        except ValueError:
            return errors.badarguments
    
    def cmd_div(self, args):
        try:
            if args[0] == "asint":
                asint = True
                args = args[1:]
            else:
                asint = False
            result = float(args[0])
            for arg in args[1:]:
                result /= float(arg)
            if asint: result = int(result)
            return result
        except ValueError:
            return errors.badarguments

def adder(registerer):
    registerer(math)

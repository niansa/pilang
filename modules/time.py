"""
This file is part of pilang.

pilang is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pilang is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pilang.  If not, see <https://www.gnu.org/licenses/>.
"""
import time as pytime
import rtypes

class time:
    def __init__(self, environ):
        self.commands = {
            "sleep": self.cmd_sleep
            }
        self.environ = environ
    
    
    def processor(self, command, args):
        try:
            return self.commands[command](args)
        except KeyError:
            return errors.nochsuchcmd
    
    def cmd_sleep(self, args):
        if len(args) != 1:
            return errors.badarguments
        try:
            pytime.sleep(float(args[0]))
        except ValueError:
            return errors.badarguments
        return rtypes.success


def adder(registerer):
    registerer(time)

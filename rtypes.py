"""
This file is part of pilang.

pilang is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pilang is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pilang.  If not, see <https://www.gnu.org/licenses/>.
"""
class success: pass
class fail:    pass

class goto(Exception):
    def __init__(self, marker, define:bool=False):
        self.define = define
        self.marker = marker

#! /usr/bin/env python3
"""
This file is part of pilang.

pilang is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pilang is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pilang.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys
from rtypes import goto
from intern import main_class

if len(sys.argv) == 1: # CLI loop
    # Show copyright note
    print("pilang  Copyright (C) 2020  niansa")
    print("This program comes with ABSOLUTELY NO WARRANTY; for details type `warranty'.")
    print("This is free software, and you are welcome to redistribute it")
    print("under certain conditions; type `license' for details.")
    print("")
    # Initialise interpreter
    main = main_class()
    while True:
        print(">>> ", end="", flush=True)
        try:
            commandstr = input()
        except EOFError:
            print()
            sys.exit(0)
        except KeyboardInterrupt:
            print()
            continue
        try:
            res = main.get_rstring(main.run_command(commandstr), errorsonly=False)
        except goto:
            res = "Goto is not possible in CLI mode"
        except RuntimeError as error:
            res = error.args[0]
        if res != None:
            print(res)
else:                  # File execution loop
    # Initialise interpreter
    main = main_class()
    # Parse argv
    if sys.argv[1] == "-c":
        del sys.argv[1]
        if len(sys.argv[1]) == 1:
            sys.exit(1)
    scriptfile = sys.argv[1]
    del sys.argv[0]
    # Save file line-by-line into list
    with open(scriptfile, mode="r") as f:
        lines = []
        for line in f:
            if line[-1] == "\n":
                line = line[:-1]
            lines.append(line)
    # Execute file
    linenum = 0
    markers = {"start": 0}
    while True:
        try:
            cmdres = main.get_rstring(main.run_command(lines[linenum]), errorsonly=True)
            linenum += 1
        except RuntimeError as error:
            print(f"In line {linenum + 1}:", error.args[0])
            sys.exit(1)
        except goto as gotoinstr:
            if gotoinstr.define:
                markers[gotoinstr.marker] = linenum + 1
                linenum += 1
                continue
            else:
                try:
                    linenum = markers[gotoinstr.marker]
                except KeyError:
                    print("Invalid goto instruction (critical)")
                    sys.exit(3)
        except IndexError:
            break
        if cmdres:
            print(cmdres)
